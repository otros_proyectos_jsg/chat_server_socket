import os
import socket
import threading


def main():
    server_address = "localhost"
    server_port = 9090

    try:
        # Conectar al servidor en el puerto 9090
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect((server_address, server_port))
        print("Te has conectado al chat server")

        def receive_messages():
            while True:
                try:
                    message = client_socket.recv(1024).decode().rstrip()
                    print(message)
                except ConnectionResetError:
                    print("La conexión con el servidor se ha cerrado.")
                    os._exit(1)  # Forzar el cierre del programa
                    break

        # Thread para leer mensajes del servidor
        reader_thread = threading.Thread(target=receive_messages)
        reader_thread.daemon = True
        reader_thread.start()

        while True:
            user_input = input() + '\n'
            if user_input:
                # Enviar mensajes al servidor
                client_socket.send(user_input.encode())

    except ConnectionRefusedError:
        print("No se pudo conectar al servidor.")
    except ConnectionError:
        print("Se perdió la conexión con el servidor.")
    except KeyboardInterrupt:
        print("Saliendo del chat.")
    finally:
        client_socket.close()


if __name__ == "__main__":
    main()
