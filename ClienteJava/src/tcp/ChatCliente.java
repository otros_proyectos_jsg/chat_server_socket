package tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;


public class ChatCliente extends Thread{


	@Override
	public void run() {
		String serverAddress = "localhost";
		int serverPort = 9090;

		try {
			// Conectar al servidor en el puerto 9090
			Socket socket = new Socket(serverAddress, serverPort);
			PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			Scanner scanner = new Scanner(System.in);

			System.out.println("Te has conectado al chat server");

			// Thread para leer mensajes del servidor
			Thread readerThread = new Thread(() -> {
				String message;
				try {
					while ((message = in.readLine()) != null) {
						System.out.println(message);
					}
				} catch (IOException e) {
					System.err.println("Se ha gestionado una excepción (" + e.getMessage() + ")");
					System.exit(1);
					//e.printStackTrace();
				}
			});
			readerThread.start();

			String userInput;
			while ((userInput = scanner.nextLine()) != null) {
				// Enviar mensajes al servidor
				out.println(userInput);
			}

			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//Constructor
	public ChatCliente(String threadName) {
		super(threadName);
	}
}
