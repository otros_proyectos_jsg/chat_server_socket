package org.example.tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Clase que representa un manejador de clientes que se ejecuta como un hilo.
 * Cada instancia de esta clase se encarga de manejar la comunicación entre el servidor
 * y un cliente específico. Permite la recepción de mensajes del cliente y el envío de
 * mensajes a ese cliente en particular.
 */
class ClientHandler extends Thread {

    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private String clientName;

    //Constructor
    public ClientHandler(Socket socket) {
        this.clientSocket = socket;
    }

    public String getClientName() {
        return clientName;
    }

    public void run() {
        try {
            // Configurar flujos de entrada y salida para el cliente
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            // Pedir al cliente que ingrese su nombre
            out.println("Introduce tu nombre: ");
            clientName = in.readLine();
            out.println("Bienvenido al chat, " + clientName);

            String message;
            while ((message = in.readLine()) != null) {
                // Difunde el mensaje a todos los clientes
                ChatServerMain.broadcastMessage(message, this);
            }
        } catch (IOException e) {
            System.err.println("Se ha gestionado una excepción (" + e.getMessage() + ")");
            //e.printStackTrace();
        } finally {
            try {
                // Cerrar recursos al finalizar
                in.close();
                out.close();
                clientSocket.close();
                ChatServerMain.clients.remove(this);
                System.out.println(clientName + " desconectado.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Envía un mensaje al cliente
     */
    public void sendMessage(String message) {
        out.println(message);
    }
}
