package org.example.tcp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**Ejemplo de un chat que utiliza sockets TCP (Transmission Control Protocol)*/
public class ChatServerMain {

    // Lista de clientes conectados
    static List<ClientHandler> clients = new ArrayList<>();

    public static void main(String[] args) {

        ServerSocket serverSocket = null;

        try {
            // Crear un servidor Socket en el puerto 9090
            serverSocket = new ServerSocket(9090);
            System.out.println("Server corriendo. Esperando clientes...");

            while (true) {
                // Esperar a que los clientes se conecten
                Socket clientSocket = serverSocket.accept();
                System.out.println("Nuevo cliente conectado: " + clientSocket);

                // Se crea un nuevo manejador para cada cliente
                ClientHandler client = new ClientHandler(clientSocket);
                clients.add(client);
                client.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                //Finalmente se cierra el socket del server
                if (serverSocket != null) {
                    serverSocket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**Transmite un mensaje a todos los clientes*/
    public static void broadcastMessage(String message, ClientHandler sender) {

        for (ClientHandler client : clients) {
            if (client != sender) {
                client.sendMessage(sender.getClientName() + ": " + message);
            }
        }
    }
}
